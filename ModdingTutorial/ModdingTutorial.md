
So you want to learn how to mod Subnautica? This tutorial will get you started wether you're a complete beginner or veteran
modder that is just looking to start on a new game. If you're new, modding does require some programming skills. This is 
not a programming tutorial so I'd recommend getting a grasp of the basics. Subnautica modding is done using C#. Against 
everything that this world has taught us with things similar alphabetically, C# is very different from C and C++ so make 
sure that you're learning the right one!  Also this tutorial is for modding on a Windows computer.

There are a few tools you'll want to download before proceeding.

First, get Visual Studio from 
https://visualstudio.microsoft.com/downloads/. The Community Edition is free for personal use and will be sufficient for
modding.  When installingmake sure to select the ".Net desktop development" option from the workloads list.

Next, you'll need Unity. You can download it from https://unity3d.com/get-unity/download/archive.  Change to the 2019.x tab
and select 2019.2.8 to match the version that Subnautica was developed on.

Third, grab dnSpy from github. https://github.com/0xd4d/dnSpy/releases   Place it in a folder somewhere convenient.

Finally, you'll need to install QModManager from the subnautica Nexus.  Here's the nexus page
Vanilla version - https://www.nexusmods.com/subnautica/mods/201/
Below Zero version - https://www.nexusmods.com/subnauticabelowzero/mods/1/
and run the executable file to install it. This will, among other things, create a QMods folder in your subnautica directory.


-----------------------
Viewing the game code 
-----------------------
This is the portion that dnSpy is used for. Run dnSpy executable and go to File->Open.  Browse to subnautica/subnautica_data/managed
and open the Assembly-CSharp.dll.  From there you can work your way down to the {} directory to view the game's classes and methods.

This is important because the way we do code injection using the Harmony library is most commonly to modify or attach code to
existing methods rather than making new things in a vacuum. Don't worry about Harmony, QModManager installed the file that 
you'll need to reference and I'll cover it further down.


----------------------------------------------
Setting up your new project in Visual Studio 
----------------------------------------------

* File -> New -> Project
* Choose 'Class Library (.NET Framework)'
* Name it and in the framework dropdown at the bottom choose '.NET Framework 4'

Once your new project is created you'll need to add some references. Right click References in the solution explorer (default on the right of your screen) and choose to Add Reference. Browse to your subnautica/Subnautica_Data/Managed directory and add Assembly-CSharp.dll, UnityEngine.dll, and UnityEngine.CoreModule.dll. From Subnautica directory also go into /BepInEx/core and add 0Harmony.dll.


-------------------
Basic Harmony Use 
-------------------
Now that your project is set up you're ready to get started. As mentioned above, the basic usage of Harmony is to to inject code into existing methods either before (Prefix) or after (Postfix). Now to tell your mod to do anything at all we need a simple command that creates the harmony instance and tell's it that patches need to be done. The MainPatcher.cs file is an example of how this file should look.  

From here you need to know what class of the game you want to patch and how. This example uses the QuickSave mod. Here I chose to patch the Update method of the Player class.  From the example you can see that we use tags inside [square brackets] to tell Harmony what to look for. [HarmonyPatch(typeOf(Player))] tells it to look at the Player class and [HarmonyPatch("Update")] tells it to look at the Update method of that class.

Next you choose whether to use a prefix or a postfix. This example uses a postfix.  Simply put the postfix tag before the method and then any code you want to run inside that method. If you are using a prefix it needs to return a boolean. True will send the code into game's method as normal, False will skip the games method, providing you with a way to overwrite the game's method entirely if you choose.  EscapePod_Patcher is provided as an example of a prefix that overwrites the game's existing method. Prefix method needs an EscapePod __instance parameter passed to it to replicate seeing 'this.xyz' in the game's code.

https://github.com/pardeike/Harmony/wiki can be a great reference to learn more about the uses of Harmony.


-------------------------------------------
Building, Testing, and Uploading Your Mod 
-------------------------------------------
When you're ready to build and test your mod right click on it in the solution explorer and select 'build'. This will generate a .dll file for your mod. By default this will be created in bin/debug but this is configurable with your visual studio setup. Now make a mod.json file to tell qModLoader how to install your mod. You can take the provided example and change it to match your own mod's requirements.

To test your mod place the .dll that you generated and mod.json into a folder with your mods name (matching the ID in mod.json).  Place this folder in the subnautica/QMods directory then start up your game. 

When you're ready to upload your mod to the nexus simply zip the folder up (that contains your mod's .dll and the mod.json file) and upload that. 


-----------
End Stuff 
-----------

Feel free to let me know if anything here was unclear or needs updating and join our discord community at
https://discord.gg/UpWuWwq. The discord community also has links to many public code repo's for existing mods that you can use for examples and inspiration.

Thanks to various members of that community for reviewing and suggesting additions to this explanation. I highly recommend you browse through some of the repos 
that they have made available linked on the discord.
