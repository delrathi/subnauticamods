﻿using Industrialization.Controllers;
using SMLHelper.V2.Assets;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Utility;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Industrialization {

    internal class MiningStation : Buildable{
        private const string NameID = "MiningStation";
        private const string HandOverKey = "UseMiningStation";
        private AssetBundle assetBundle;

        public CraftTree.Type TreeTypeID { get; private set; }
        public override TechGroup GroupForPDA { get; } = TechGroup.ExteriorModules;
        public override TechCategory CategoryForPDA { get; } = TechCategory.ExteriorModule;
        public override string AssetsFolder { get; } = "Industrialization/Assets";

        private GameObject _Prefab;
        private MiningStationController miningStationController;

        private string material = "Titanium";
        private int amount = 0;

        internal MiningStation()
            : base(NameID,
                 "Mining Station", 
                 "A specialized mining drill to automatically extract resources.")
        {
            OnStartedPatching += () =>
            {
                Console.WriteLine("Industrialization - Beginning to Patch MiningStation");
                // Ensure any related parts are patched here - none in this case
            };

            OnFinishedPatching += () =>
            {
                Console.WriteLine("Industrialization: Finishing patching MiningStation");
                LanguageHandler.SetLanguageLine(HandOverKey, "Use Mining Station");
               // this.TreeTypeID = CraftTree.Type.Constructor; // assumption: constructor = hab builder
                //this.TreeTypeID = CreateCustomTree();
            };
        }

        // Eventually will want a fully custom tree, but for this point just adding it to as a standalone will suffice
        //private CraftTree.Type CreateCustomTree()
        //{
        //    ModCraftTreeRoot rootNode = CraftTreeHandler.CreateCustomCraftTreeAndType(NameID, out CraftTree.Type craftType);
        //}

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData()
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.Titanium, 1),
                }
            };
        }

        private bool GetPrefabs()
        {
            var path = @"./QMods/Industrialization/Assets/miningstation";
            if(assetBundle == null)
            {
                assetBundle = AssetBundle.LoadFromFile(path);
            }
            if (assetBundle == null)
            {
                Console.WriteLine("ERROR: Industrialization: Cannot locate asset bundle.");
                return false;
            }
            
            GameObject prefab = assetBundle.LoadAsset<GameObject>("MiningStation");
            if (prefab != null)
            {
                _Prefab = prefab;

                return true;
            }

            Console.WriteLine("ERROR: Industrialization: Prefab not found, Drill");
            return false;
        }

        public override GameObject GetGameObject()
        {
            GameObject prefab = null;

            if (!GetPrefabs()) return null;

            try
            {
                prefab = GameObject.Instantiate(_Prefab);
                //========== Allows the building animation and material colors ==========// 
                Shader shader = Shader.Find("MarmosetUBER");
                Renderer[] renderers = prefab.GetComponentsInChildren<Renderer>();
                SkyApplier skyApplier = prefab.AddComponent<SkyApplier>();
                skyApplier.renderers = renderers;
                skyApplier.anchorSky = Skies.Auto;

                //========== Allows the building animation and material colors ==========// 

                // Add constructible
                var constructable = prefab.AddComponent<Constructable>();
                constructable.allowedOnWall = false;
                constructable.allowedOnGround = true;
                constructable.allowedInSub = false;
                constructable.allowedInBase = false;
                constructable.allowedOnCeiling = false;
                constructable.allowedOutside = true;
                constructable.model = prefab.FindChild("model");
                constructable.techType = TechType;
                constructable.rotationEnabled = true;

                // Add large world entity ALLOWS YOU TO SAVE ON TERRAIN
                var lwe = prefab.AddComponent<LargeWorldEntity>();
                lwe.cellLevel = LargeWorldEntity.CellLevel.Global;

                prefab.AddComponent<PrefabIdentifier>().ClassId = this.ClassID;
                prefab.AddComponent<TechTag>().type = TechType;
                miningStationController = prefab.AddComponent<MiningStationController>();
                if(miningStationController == null)
                {
                    Console.WriteLine("YOLO - failed to add component");
                }
                else
                {
                    Console.WriteLine("YOLO - successfully added component");
                }
                miningStationController.parent = prefab;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return prefab;
        }

        public String GetMaterial()
        {
            return material;
        }

        public int GetAmount()
        {
            return amount;
        }
    }
}
