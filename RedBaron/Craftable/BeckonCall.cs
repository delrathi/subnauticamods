﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Assets;
using UnityEngine;
using System.Collections;

namespace RedBaron {
    public class BeckonCall : Craftable {

        private static readonly BeckonCall main = new BeckonCall();
        private BeckonCallTool pt;

        internal static TechType TechTypeID { get; private set; }

        public BeckonCall() : base("BeckonCall", "Beckon Call Remote", "Summons your beckon call upgraded Seamoth.")
        {
            OnFinishedPatching += AdditionalPatching;
        }

        public override CraftTree.Type FabricatorType { get; } = CraftTree.Type.Fabricator;
        public override TechGroup GroupForPDA { get; } = TechGroup.Personal;
        public override TechCategory CategoryForPDA { get; } = TechCategory.Tools;
        public override string AssetsFolder { get; } = "RedBaron/Assets";
        public override string[] StepsToFabricatorTab { get; } = new[] { "Personal", "Tools" };
        public override TechType RequiredForUnlock { get; } = TechType.None;

        public override GameObject GetGameObject()
        {
            GameObject prefab = GameObject.Instantiate(CraftData.GetPrefabForTechType(TechType.AirBladder));
            GameObject.DestroyImmediate(prefab.GetComponent<PlayerTool>());
            pt = prefab.AddComponent<BeckonCallTool>();
            pt.SetSFX(prefab.AddComponent<FMOD_CustomEmitter>(), ScriptableObject.CreateInstance<FMODAsset>());
            return prefab;

        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.ComputerChip, 1),
                    new Ingredient(TechType.Glass, 1)
                }
            };
        }

        public static void PatchSMLHelper()
        {
            main.Patch();
        }

        private void AdditionalPatching()
        {
            TechTypeID = this.TechType;
            CraftDataHandler.SetEquipmentType(this.TechType, EquipmentType.Hand);
        }
    }

    public class BeckonCallTool: PlayerTool {

        public FMOD_CustomEmitter sfx;
        public FMODAsset soundEffect;

        //public override string animToolName {
        //    get {
        //        return "airbladder";
        //    }
        //}

        public void SetSFX(FMOD_CustomEmitter emitter, FMODAsset asset)
        {
            sfx = emitter;
            soundEffect = asset;

            soundEffect.path = "event:/sub/cyclops/shield_on_loop";
            sfx.asset = soundEffect;
            sfx.followParent = true;
        }

        public override bool OnRightHandDown()
        {
            SeaMoth[] mothList = GameObject.FindObjectsOfType<SeaMoth>();
            float i = 0f;
            // Summon all upgraded Seamoth's. Multiples will appear above the first.
            foreach (SeaMoth moth in mothList)
            {
                if (moth.modules.GetCount(SeamothBeckonCallModule.TechTypeID) > 0)
                {                    
                    Vector3 pos = 10f * Player.main.camRoot.GetAimingTransform().forward.normalized;
                    Vector3 newPos = Player.main.transform.position + pos;
                    newPos.y += 10 * i;
                    StartCoroutine(Delay(moth, newPos, sfx));

                    i++;
                }
            }
            return true;
        }

        IEnumerator Delay(SeaMoth seaMoth, Vector3 newPos, FMOD_CustomEmitter sfx)
        {
            sfx.Play();
            yield return new WaitForSeconds(10);
            seaMoth.transform.position = newPos;
            sfx.Stop();
        }
    }
}
