﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;

namespace RedBaron {

    [HarmonyPatch(typeof(CyclopsHelmHUDManager))]
    [HarmonyPatch("Update")]
    public class CyclopsHelmHUDManager_Update_Patch {

        [HarmonyPostfix]
        static void Postfix(CyclopsHelmHUDManager __instance)
        {

            if (__instance.subLiveMixin.IsAlive())
            {
                int num = (int)__instance.transform.position.y;
                if(num > 0)
                {
                    Color color = Color.white;
                    __instance.depthText.text = string.Format("Altitude: {0}m", num);
                    __instance.depthText.color = color;
                }
            }

            return;

        }
    }
}