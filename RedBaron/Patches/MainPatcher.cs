﻿using Harmony;
using System.Reflection;
using UnityEngine;
using System;
using MoreCyclopsUpgrades.API;
using SMLHelper.V2.Options;
using SMLHelper.V2.Handlers;


namespace RedBaron {
    public class MainPatcher {

        public static float originalGrav = 9.81f;
        public static Survival surv;

        public static void Patch()
        {
            Console.WriteLine("RedBaron: Patching upgrades");
            SeamothFlightModule.PatchSMLHelper();
            SeamothManeuveringJetsModule.PatchSMLHelper();
            SeamothLifeSupportModule.PatchSMLHelper();
            SeamothBeckonCallModule.PatchSMLHelper();
            BeckonCall.PatchSMLHelper();

            //SeamothRotaryGunsModule.PatchSMLHelper();

            var cyclopsFlightUpgrade = new CyclopsFlightModule();
            cyclopsFlightUpgrade.Patch();

            MCUServices.Register.CyclopsUpgradeHandler(cyclopsFlightUpgrade.CreateFlightUpgradeHandler);

            Console.WriteLine("RedBaron: Patching complete");
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.redbaron.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());

           
        }
        

        public class RedBaronConfig : MonoBehaviour {
            //Placeholder for when configs are added.
        }
    }
}