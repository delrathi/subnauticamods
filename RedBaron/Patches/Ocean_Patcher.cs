﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;

namespace RedBaron {

    [HarmonyPatch(typeof(Ocean))]
    [HarmonyPatch("GetDepthOf")]
    public class Ocean_GetDepthOf_Patch {

        [HarmonyPostfix]
        static void Postfix(GameObject obj, ref float __result)
        {
            if (Player.main.isPiloting)
            {
                if (obj.name == "Cyclops-MainPrefab(Clone)")
                {
                    if(__result <= 0f)
                    {
                        __result = 5f;
                    }
                }
            }

            return;
            
        }
    }
}