﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Harmony;

namespace RedBaron {

    [HarmonyPatch(typeof(Vehicle))]
    [HarmonyPatch("StabilizeRoll")]
    internal class Vehicle_StabilizeRoll_Patcher {
        [HarmonyPrefix]
        public static bool Prefix(Vehicle __instance)
        {
            if (__instance.name.Equals("SeaMoth(Clone)") && __instance.modules.GetCount(SeamothManeuveringJetsModule.TechTypeID) > 0)
            {
                return false;
            }
            return true;
        }
    }
    

}
