﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Assets;
using UnityEngine;

namespace RedBaron {
    public class SeamothBeckonCallModule : Craftable {

        private static readonly SeamothBeckonCallModule main = new SeamothBeckonCallModule();
        public SeaMoth seaMoth;

        internal static TechType TechTypeID { get; private set; }

        public SeamothBeckonCallModule() : base("SeamothBeckonCallModule", "Seamoth Beckon Call", "Installs autopilot system to allow the Seamoth to be summoned to it's owner.")
        {
            OnFinishedPatching += AdditionalPatching;
        }

        public override CraftTree.Type FabricatorType { get; } = CraftTree.Type.SeamothUpgrades;
        public override TechGroup GroupForPDA { get; } = TechGroup.VehicleUpgrades;
        public override TechCategory CategoryForPDA { get; } = TechCategory.VehicleUpgrades;
        public override string AssetsFolder { get; } = "RedBaron/Assets";
        public override string[] StepsToFabricatorTab { get; } = new[] { "SeamothModules" };
        public override TechType RequiredForUnlock { get; } = TechType.None;

        public override GameObject GetGameObject()
        {
            GameObject prefab = CraftData.GetPrefabForTechType(TechType.SeamothSonarModule);
            return GameObject.Instantiate(prefab);

        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.AdvancedWiringKit, 2),
                    new Ingredient(TechType.Magnetite, 3)
                }
            };
        }

        public static void PatchSMLHelper()
        {
            main.Patch();
        }

        private void AdditionalPatching()
        {
            TechTypeID = this.TechType;
            CraftDataHandler.SetEquipmentType(this.TechType, EquipmentType.SeamothModule);
        }
    }


}
