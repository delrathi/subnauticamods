﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoreCyclopsUpgrades.API;
using MoreCyclopsUpgrades.API.Upgrades;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Crafting;
using UnityEngine;

namespace RedBaron {
    internal class CyclopsFlightModule : CyclopsUpgrade{

        internal static TechType TechTypeID { get; private set; }

        public CyclopsFlightModule() :
            base("CyclopsFlightModule",
                "Cyclops Anti-Grav Generators",
                "Upgrades the Cyclops submersible with Anti-Gravity technology to allow for limited flight.")
        {

            OnFinishedPatching += AdditionalPatching;

        }

        public override CraftTree.Type FabricatorType { get; } = CraftTree.Type.CyclopsFabricator;
        public override string AssetsFolder { get; } = "RedBaron/Assets";
        public override string[] StepsToFabricatorTab { get; } = MCUServices.CrossMod.StepsToCyclopsModulesTabInCyclopsFabricator;

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData()
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.Aerogel, 4),
                    new Ingredient(TechType.PrecursorIonCrystal, 2),
                    new Ingredient(TechType.Polyaniline, 2)
                }
            };
        }
        
        internal FlightHandler CreateFlightUpgradeHandler(SubRoot cyclops)
        {
            return new FlightHandler(this, cyclops);
        }

        private void AdditionalPatching()
        {
            TechTypeID = this.TechType;
        }
        

    }
}
