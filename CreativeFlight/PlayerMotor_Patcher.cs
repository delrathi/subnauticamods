﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;

namespace CreativeFlight {
    [HarmonyPatch(typeof(PlayerMotor))]
    [HarmonyPatch("Start")]
    internal class PlayerMotor_Start_Patch {
        [HarmonyPostfix]
        public static void Postfix(PlayerMotor __instance)
        {
            if (!GameModeUtils.IsOptionActive(GameModeOption.Creative))
            {
                return;
            }
            __instance.usingGravity = false;
            __instance.gravity = 0.0f;

        }
    }

    [HarmonyPatch(typeof(PlayerMotor))]
    [HarmonyPatch("Update")]
    internal class PlayerMotor_Update_Patch {
        [HarmonyPostfix]
        public static void Postfix(PlayerMotor __instance)
        {
            if (Player.main.IsInBase() || Player.main.IsInSubmarine() || Player.main.IsInSub() || Player.main.IsInside() || Player.main.IsInsideWalkable() || Player.main.IsSwimming())
            {
                return;
            }
            if (!GameModeUtils.IsOptionActive(GameModeOption.Creative))
            {
                return;
            }
            __instance.SetVelocity(Vector3.zero);
        }
    }
}
