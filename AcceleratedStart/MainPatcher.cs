﻿using System.Reflection;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Options;
using Harmony;
using System;
using UnityEngine;
using System.Collections.Generic;

namespace AcceleratedStart
{
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.acceleratedstart.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
            OptionsPanelHandler.RegisterModOptions(new Options("Accelerated Start"));
        }

    }

    public class Options : ModOptions {
        List<string> slidersToAdd = new List<string>();

        public Options(string name) : base(name)
        {

            try
            {
                SliderChanged += OnSliderChanged;
                ToggleChanged += OnToggleChanged;
                ChoiceChanged += OnChoiceChanged;
            }
            catch (Exception e)
            {

            }
        }

        public void InitializeSliders()
        {

            var path = @"./QMods/AcceleratedStart/config.txt";
            string line;
            System.IO.StreamReader file = new System.IO.StreamReader(path);
            line = file.ReadLine();
            // Initialize the custom inventory sliders
            while (!line.Contains("{"))
            {
                line = file.ReadLine();
            }
            line = file.ReadLine();
            while (!line.Contains("}"))
            {
                slidersToAdd.Add(line);
                line = file.ReadLine();
            }
            file.Close();
        }

        public override void BuildModOptions()
        {
            try
            {
                InitializeSliders();
                AddChoiceOption("acceleratedstart_defaultLoadoutOptions", "Initial Loadout Selection", new string[] { "Vanilla", "Accelerated Default", "Real Alterra", "Realistic Survival", "None" }, 0);
                AddToggleOption("defaultLoadout", "Default Supplies", true);
                AddToggleOption("Terraformer", "Terraformer", false);
                AddSliderOption("Titanium", "Titanium", 0, 40, 0);
                AddSliderOption("Aerogel", "Aerogel", 0, 40, 0);
                AddSliderOption("Copper", "Copper", 0, 40, 0);
                AddSliderOption("Battery", "Batteries", 0, 10, 0);
                AddSliderOption("Quartz", "Quartz", 0, 40, 0);
                foreach(string item in slidersToAdd)
                {
                    AddSliderOption(item, item, 0, 20, 0);
                }
            }
            catch (Exception e)
            {

            }
        }

        public void OnToggleChanged(object sender, ToggleChangedEventArgs e)
        {
            try
            {
                string id = e.Id;
                switch (id)
                {
                    case "Terraformer":
                        AcceleratedStartConfig.haveTerraformer = e.Value;
                        break;
                    default:
                        Console.WriteLine("ERROR: AcceleratedStart invalid item toggle");
                        break;
                }

            }catch (Exception ex)
            {
                Console.WriteLine("ERROR: AcceleratedStart onToggleChanged for " + e.Id);
            }
        }

        public void OnSliderChanged(object sender, SliderChangedEventArgs e)
        {
            try
            {
                int val = (int)Math.Round(e.Value, 0);
                AcceleratedStartConfig.UpdateConfig(e.Id, val);
            }
            catch (Exception ex)
            {
                Console.WriteLine("ERROR: AcceleratedStart onSliderChanged for " + e.Id);
            }
        }

        public void OnChoiceChanged(object sender, ChoiceChangedEventArgs e)
        {
            if (e.Id.Equals("acceleratedstart_defaultLoadoutOptions"))
            {
                AcceleratedStartConfig.initialLoadout = e.Index;
            }
        }
    }

    public class AcceleratedStartConfig : MonoBehaviour {
        public static Dictionary<string, int> startingItems = new Dictionary<string, int>();

        public static bool haveTerraformer = false;
        public static int initialLoadout = 1;
        public void Start()
        {

            
        }

        public static void UpdateConfig(string id, int value)
        {
            if (startingItems.ContainsKey(id))
            {
                startingItems[id] = value;
            }
            else
            {
                startingItems.Add(id, value);
            }
        }

    }
}
