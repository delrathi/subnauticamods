﻿using System;
using System.Collections.Generic;
using Harmony;

namespace AcceleratedStart
{
    [HarmonyPatch(typeof(LootSpawner))]
    [HarmonyPatch("GetEscapePodStorageTechTypes")]
    class podChest_Patch
    {
        [HarmonyPrefix]
        public static bool Prefix(LootSpawner __instance)
        {
            switch (AcceleratedStartConfig.initialLoadout)
            { 

                // Vanilla Loadout
                case 0:
                    break;

                // Accelerated Start Default
                case 1:
                    __instance.escapePodTechTypes.Add(TechType.Builder);
                    __instance.escapePodTechTypes.Add(TechType.Knife);
                    __instance.escapePodTechTypes.Add(TechType.Tank);
                    __instance.escapePodTechTypes.Add(TechType.Fins);
                    __instance.escapePodTechTypes.Add(TechType.Flashlight);
                    __instance.escapePodTechTypes.Add(TechType.Scanner);
                    __instance.escapePodTechTypes.Add(TechType.Rebreather);
                    __instance.escapePodTechTypes.Add(TechType.Welder);

                    __instance.escapePodTechTypes.Add(TechType.Battery);
                    __instance.escapePodTechTypes.Add(TechType.Battery);
                    __instance.escapePodTechTypes.Add(TechType.Battery);
                    __instance.escapePodTechTypes.Add(TechType.Battery);

                    __instance.escapePodTechTypes.Add(TechType.Quartz);
                    __instance.escapePodTechTypes.Add(TechType.Quartz);
                    __instance.escapePodTechTypes.Add(TechType.Quartz);
                    __instance.escapePodTechTypes.Add(TechType.Quartz);

                    __instance.escapePodTechTypes.Add(TechType.Copper);

                    __instance.escapePodTechTypes.Add(TechType.Gold);
                    __instance.escapePodTechTypes.Add(TechType.JeweledDiskPiece);

                    for (int i = 0; i < 10; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.Titanium);
                    }

                    __instance.escapePodTechTypes.Add(TechType.RadiationGloves);
                    __instance.escapePodTechTypes.Add(TechType.RadiationSuit);
                    __instance.escapePodTechTypes.Add(TechType.RadiationHelmet);
                    break;

                // Real Alterra (FatherSarge)
                case 2:
                    __instance.escapePodTechTypes.Clear();
                    __instance.escapePodTechTypes.Add(TechType.Knife);
                    __instance.escapePodTechTypes.Add(TechType.Flashlight);
                    __instance.escapePodTechTypes.Add(TechType.Scanner);
                    __instance.escapePodTechTypes.Add(TechType.Welder);
                    __instance.escapePodTechTypes.Add(TechType.LaserCutter);
                    __instance.escapePodTechTypes.Add(TechType.Builder);

                    for (int i = 0; i < 7; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.Battery);
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.Flare);
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.FilteredWater);
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.NutrientBlock);
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.FirstAidKit);
                    }
                    break;

                // Realistic Survival (SilverThorn12345)
                case 3:
                    __instance.escapePodTechTypes.Clear();
                    __instance.escapePodTechTypes.Add(TechType.Knife);
                    __instance.escapePodTechTypes.Add(TechType.Flashlight);
                    __instance.escapePodTechTypes.Add(TechType.Scanner);
                    __instance.escapePodTechTypes.Add(TechType.Welder);
                    __instance.escapePodTechTypes.Add(TechType.LaserCutter);
                    __instance.escapePodTechTypes.Add(TechType.Compass);
                    __instance.escapePodTechTypes.Add(TechType.RepulsionCannon);
                    __instance.escapePodTechTypes.Add(TechType.Builder);
                    __instance.escapePodTechTypes.Add(TechType.Flare);
                    __instance.escapePodTechTypes.Add(TechType.Flare);

                    for (int i = 0; i < 6; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.Battery);
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.BigFilteredWater);
                    }

                    for (int i = 0; i < 8; i++)
                    {
                        __instance.escapePodTechTypes.Add(TechType.NutrientBlock);
                    }
                    break;

                case 4:
                    __instance.escapePodTechTypes.Clear();
                    break;

                default:
                    Console.WriteLine("ERROR: AcceleratedStart.LootSpawner_patch.  Invalid default loadout selected. Using vanilla supplies.");
                    break;
            }
                // Add items selected by mod menu config
                if (AcceleratedStartConfig.haveTerraformer)
            {
                __instance.escapePodTechTypes.Add(TechType.Terraformer);
            }

            foreach (KeyValuePair<string, int> keyValue in AcceleratedStartConfig.startingItems)
            {
                string key = keyValue.Key;
                int value = keyValue.Value;
                for(int i = 0; i < value; i++)
                {
                    Console.WriteLine("AcceleratedStart: Adding " + value + " " + key + " to starting supplies.");
                    TechType item = (TechType)Enum.Parse(typeof(TechType), key);
                   __instance.escapePodTechTypes.Add(item);
                   
                }
            }
            return true;
        }
    }
}
