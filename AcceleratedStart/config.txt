﻿RepairRadio: true
RepairPod  : true
 
// For additional sliders add them to this list below this line and matching the format. Capitalization matters.
Additional Slider Types{
Sulphur
Silicone
}

A large list of possible sliders to add to the above - Note that I have NOT personally tested every one of these so if you
put in something weird and it breaks the mod or your game just remove it.  This is also not an exhaustive list. If a specific 
item that you want is missing feel free to message me or ask in the subnautica modding reddit or discord channels.

-------------\
Raw Resources |
-------------/
Benzene
Copper
Diamond
Fiber
Glass
Kyanite
Lead
Lithium
Magnesium
Nickel
Quartz
SeaTreaderPoop
Silicone
Silver
StalkerTooth
Sulphur
Titanium
Uranium

------------------\
Crafting Materials |
------------------/
AdvancedWiringKit
Aerogel
Bleach
ComputerChip
CopperWire
EnameledGlass
FiberMesh
Lubricant
PlasteelIngot
ScrapMetal
TitaniumIngot
WiringKit

----------------\
Tools & Supplies |
----------------/
AirBladder
Battery
Beacon
Builder
Compass
DoubleTank
Fins
FireExtinguisher
FirstAidKit
Flare
Flashlight
Gravsphere
HeatBlade
HighCapacityTank
JeweledDiskPiece
Knife
LaserCutter
LithiumIonBattery
MapRoomHUDChip
Pipe
PlasteelTank
PowerCell
PropulsionCannon
RadiationGloves
RadiationHelmet
RadiationSuit
Rebreather
ReinforcedDiveSuit
RepulsionCannon
Scanner
Seaglide
StasisRifle
Stillsuit
SwimChargeFins
Tank
Terraformer
UltraGlideFins
Welder

----------\
Decorative |
----------/
Cap1
Cap2
PosterAurora
PosterExosuit1
PosterExosuit2
PosterKitty

------------\
Food & Water |
------------/
BigFilteredWater
Coffee
CookedBladderfish
CookedBoomerang
CookedEyeye
CookedGarryFish
CookedHoleFish
CookedHoopfish
CookedHoverfish
CookedLavaBoomerang
CookedLavaEyeye
CookedOculus
CookedPeeper
CookedReginald
CookedSpadefish
CookedSpinefish
CuredBladderfish
CuredBoomerang
CuredEyeye
CuredGarryFish
CuredHoleFish
CuredHoopfish
CuredHoverfish
CuredLavaBoomerang
CuredLavaEyeye
CuredOculus
CuredPeeper
CuredReginald
CuredSpadefish
CuredSpinefish
DisinfectedWater
FilteredWater
NutrientBlock
Snack1
Snack2
Snack3
StillsuitWater

--------------------\
Friends & Companions |
--------------------/
BonesharkEgg
CrabsnakeEgg
CrabsquidEgg
CrashEgg
CutefishEgg
GasopodEgg
GenericEgg
GrandReefsEgg
GrassyPlateausEgg
JellyrayEgg
JumperEgg
KelpForestEgg
KooshZoneEgg
LavaLizardEgg
LavaZoneEgg
MesmerEgg
MushroomForestEgg
RabbitrayEgg
ReefbackEgg
SafeShallowsEgg
SandsharkEgg
ShockerEgg
SpadefishEgg
StalkerEgg
TwistyBridgesEgg