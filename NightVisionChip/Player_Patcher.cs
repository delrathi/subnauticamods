﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Harmony;
using UnityEngine;

namespace MAC.NightVisionChip {

    [HarmonyPatch(typeof(Player))]
    [HarmonyPatch("Update")]
    internal class Player_Update_Patcher {
        [HarmonyPostfix]
        public static void Postfix()
        {
            if (Input.GetKeyUp(NightVisionChipConfig.nightvision_toggle_key)){
                NightVisionChipConfig.nightVisionEnabled = !NightVisionChipConfig.nightVisionEnabled;
                ToggleNightVision();
            }
        }

        static void ToggleNightVision()
        {
            if (MainPatcher.ambientLight == null)
            {
                MainPatcher.ambientIntensity = RenderSettings.ambientIntensity;
                MainPatcher.ambientLight = RenderSettings.ambientLight;
            }

            if (Inventory.main.equipment.GetCount(NightVisionChip.TechTypeID) <= 0)
            {

                RenderSettings.ambientIntensity = MainPatcher.ambientIntensity;
                RenderSettings.ambientLight = MainPatcher.ambientLight;
            }
            else
            {
                if (NightVisionChipConfig.nightVisionEnabled)
                {
                    RenderSettings.ambientIntensity = 100f;
                    RenderSettings.ambientLight = Color.green;
                }
                else
                {
                    RenderSettings.ambientIntensity = MainPatcher.ambientIntensity;
                    RenderSettings.ambientLight = MainPatcher.ambientLight;
                }
            }
        }
    }
}
