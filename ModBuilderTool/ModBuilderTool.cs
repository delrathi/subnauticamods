﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Assets;
using UnityEngine;
using System.Collections;
using System.Reflection;

namespace ModBuilderTool {
    public class ModBuilderTool : Craftable {

        private static readonly ModBuilderTool main = new ModBuilderTool();
        private ModBuilderToolTool pt;

        internal static TechType TechTypeID { get; private set; }

        public ModBuilderTool() : base("ModBuilderTool", "Modded Builder Tool", "A habitat builder for modded items.")
        {
            OnFinishedPatching += AdditionalPatching;
        }

        public override CraftTree.Type FabricatorType { get; } = CraftTree.Type.Fabricator;
        public override TechGroup GroupForPDA { get; } = TechGroup.Personal;
        public override TechCategory CategoryForPDA { get; } = TechCategory.Tools;
        public override string AssetsFolder { get; } = "ModBuilderTool/Assets";
        public override string[] StepsToFabricatorTab { get; } = new[] { "Personal", "Tools" };
        public override TechType RequiredForUnlock { get; } = TechType.None;

        public override GameObject GetGameObject()
        {
            GameObject prefab = CraftData.GetPrefabForTechType(TechType.Builder);
            
            //Console.WriteLine("YOLO: prefab name: " + prefab.name);
            //PlayerTool toolCheck = prefab.GetComponent<PlayerTool>();
            //if(toolCheck == null)
            //{
            //    Console.WriteLine("YOLO: could not find playertool component");
            //}
            //else
            //{
            //    Console.WriteLine("YOLO - found playertool component");

            GameObject.Destroy(prefab.GetComponent<PlayerTool>());
            //}
            
            pt = prefab.AddComponent<ModBuilderToolTool>();

            return GameObject.Instantiate(prefab);

        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.Sulphur, 1)
                }
            };
        }

        public static void PatchSMLHelper()
        {
            main.Patch();
        }

        private void AdditionalPatching()
        {
            TechTypeID = this.TechType;
            CraftDataHandler.SetEquipmentType(this.TechType, EquipmentType.Hand);
        }
    }

    public class ModBuilderToolTool : PlayerTool {


        public override bool OnRightHandDown()
        {
            Console.WriteLine("YOLO, right hand down with modded tool. You should NOT still see the normal menu");

            uGUI_BuilderMenu menu;
            Console.WriteLine("YOLO 1");
            FieldInfo reflectionInfo = typeof(uGUI_BuilderMenu).GetField("singleton");
            if(reflectionInfo == null)
            {
                Console.WriteLine("YOLO - got a null singleton boss. Trying to force an awaken...");
                uGUI_BuilderMenu.Show();
                uGUI_BuilderMenu.Hide();
                reflectionInfo = typeof(uGUI_BuilderMenu).GetField("singleton");
            }

            if(reflectionInfo == null)
            {
                Console.WriteLine("YOLO - failed to force singleton. Going to error now.");
            }
            
            Console.WriteLine("YOLO 2");
            object obj = reflectionInfo.GetValue(null);
            Console.WriteLine("YOLO 3");
            menu = (uGUI_BuilderMenu)obj;
            Console.WriteLine("YOLO 4");
            var wazzit = (uGUI_BuilderMenu)typeof(uGUI_BuilderMenu).GetMethod("UpdateItems").Invoke(null, null);
            Console.WriteLine("YOLO 5");
            //uGUI_BuilderMenu.Show();

            return true;
        }

    }
}
