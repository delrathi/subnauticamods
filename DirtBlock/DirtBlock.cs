﻿using DirtBlock.Controllers;
using SMLHelper.V2.Assets;
using SMLHelper.V2.Crafting;
using SMLHelper.V2.Handlers;
using SMLHelper.V2.Utility;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace DirtBlock {
    internal class DirtBlock : Buildable {
        private const string NameID = "DirtBlock";

        public CraftTree.Type TreeTypeID { get; private set; }
        public override TechGroup GroupForPDA { get; } = TechGroup.ExteriorModules;
        public override TechCategory CategoryForPDA { get; } = TechCategory.ExteriorModule;
        public override string AssetsFolder { get; } = "DirtBlock/Assets";

        private GameObject _Prefab;

        internal DirtBlock()
            : base(NameID,
                  "Dirt Block",
                  "A strangely cubical and low res dirt block.")
        {
            OnStartedPatching += () =>
            {
                Console.WriteLine("DirtBlock: Beginning to patch DirtBlock");
            };

            OnFinishedPatching += () =>
            {
                Console.WriteLine("DirtBlock: Finishing patching DirtBlock");
            };
        }

        protected override TechData GetBlueprintRecipe()
        {
            return new TechData()
            {
                craftAmount = 1,
                Ingredients =
                {
                    new Ingredient(TechType.Titanium, 1),
                }
            };
        }

        private bool GetPrefabs()
        {

            GameObject prefab = MainPatcher.assetBundle.LoadAsset<GameObject>("dirtblock");
            if(prefab != null)
            {
                _Prefab = prefab;
                return true;
            }

            Console.WriteLine("ERROR: DirtBlock: Prefab not found, DirtBlock");
            return false;
        }

        public override GameObject GetGameObject()
        {
            GameObject prefab = null;

            if (!GetPrefabs()) return null;

            try
            {
                prefab = GameObject.Instantiate(_Prefab);
                //========== Allows the building animation and material colors ==========// 
                Shader shader = Shader.Find("MarmosetUBER");
                Renderer[] renderers = prefab.GetComponentsInChildren<Renderer>();
                SkyApplier skyApplier = prefab.AddComponent<SkyApplier>();
                skyApplier.renderers = renderers;
                skyApplier.anchorSky = Skies.Auto;

                //========== Allows the building animation and material colors ==========// 

                // Add constructible
                var constructable = prefab.AddComponent<Constructable>();
                constructable.allowedOnWall = true;
                constructable.allowedOnGround = true;
                constructable.allowedInSub = false;
                constructable.allowedInBase = false;
                constructable.allowedOnCeiling = false;
                constructable.allowedOutside = true;
                constructable.model = prefab.FindChild("model");
                constructable.techType = TechType;
                constructable.rotationEnabled = true;
                constructable.allowedOnConstructables = true;

                // Add large world entity ALLOWS YOU TO SAVE ON TERRAIN
                var lwe = prefab.AddComponent<LargeWorldEntity>();
                lwe.cellLevel = LargeWorldEntity.CellLevel.Near;

                prefab.AddComponent<PrefabIdentifier>().ClassId = this.ClassID;
                prefab.AddComponent<TechTag>().type = TechType;
                DirtBlockController controller = prefab.AddComponent<DirtBlockController>();

                if (controller == null)
                {
                    Console.WriteLine("ERROR: DirtBlock: failed to add controller");
                }

                //miningStationController = prefab.AddComponent<MiningStationController>();
                //if (miningStationController == null)
                //{
                //    Console.WriteLine("YOLO - failed to add component");
                //}
                //else
                //{
                //    Console.WriteLine("YOLO - successfully added component");
                //}
                //miningStationController.parent = prefab;

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            prefab.transform.position.Set((float)Math.Round(prefab.transform.position.x), (float)Math.Round(prefab.transform.position.y), (float)Math.Round(prefab.transform.position.z));
            return prefab;
        }

    }
}

